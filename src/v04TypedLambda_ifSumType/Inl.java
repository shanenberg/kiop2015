/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package v04TypedLambda_ifSumType;

import java.util.Set;

public class Inl extends Expression {

	public SumType sumType;
	public Expression body;
	
	public Inl(SumType sumType, Expression body) {
		this.sumType = sumType;
		this.body = body;
	}

	/*                    t1 -> t'
	 *   ----------------------------------
	 *   inl (T) t1 -> inl (T) t' 
	 * 
	 */
	public Expression reduce() {
		if (body.isReducible())
			body = body.reduce();
		return this;
	}

	@Override
	public Expression substituteWith(String aName, Expression exp) {
		body = body.substituteWith(aName, exp);
		return this;
	}

	@Override
	public boolean isReducible() {
		return body.isReducible();
	}

	@Override
	public Set FI() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 *     E |- t: T1
	 *     =========================
	 * 		E |- inl(T1+T2) t: T1+T2
	 */
	@Override
	public Type getType(Environment e) {
		Type t = body.getType(e);
		if (!sumType.left.equals(t)) throw new RuntimeException("blabla");
		
		return sumType;
		
	}

}
