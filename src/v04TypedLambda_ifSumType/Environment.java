/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package v04TypedLambda_ifSumType;

import java.util.HashMap;

public class Environment {
	public HashMap<String, Type> env = new HashMap<String, Type>();
	
	public static Environment createEnvironment() {
		Environment e = new Environment();
		e.env.put("true", new Boolean());
		e.env.put("false", new Boolean());
		e.env.put("0", new Number());
		e.env.put("1", new Number());
		e.env.put("2", new Number());
		e.env.put("not", new FunctionType(new  Boolean(),new  Boolean()));
		e.env.put("largerThan0", new FunctionType(new  Number(),new  Boolean()));
		return e;
	}
	
	public Environment clone() {
		Environment e = new Environment();
		HashMap<String, Type> env2 = (HashMap<String, Type>) env.clone();
		e.env = env2;
		return e;
	}
	
}
