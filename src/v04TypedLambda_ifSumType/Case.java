package v04TypedLambda_ifSumType;
import java.util.HashSet;
import java.util.Set;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

public class Case extends Expression {
	


	public Case(Expression sumExpression, SumType sumType,
			Variable inlVariable, Expression inlExpression,
			Variable inrVariable, Expression inrExpression) {
		super();
		this.sumExpression = sumExpression;
		this.sumType = sumType;
		this.inlVariable = inlVariable;
		this.inlExpression = inlExpression;
		this.inrVariable = inrVariable;
		this.inrExpression = inrExpression;
	}

	@Override
	public Expression reduce() {
		throw new RuntimeException("not yet implemented");
	}
	
	public Expression reduceWith(Expression appliedParameter) {
		throw new RuntimeException("not yet implemented");
	}	
	
	public Set FI() {
		throw new RuntimeException("not yet implemented");
	}

	@Override
	public Expression substituteWith(String aName, Expression exp) {
		throw new RuntimeException("not yet implemented");
	}

	@Override
	public boolean isReducible() {
		return false;
	}

	/**
	 *          E, inlVar: T1 |- inlExpression: T   
	 *          E, inrVar: T2 |- inrExpression: T
	 * T-Abs =====================================================================
	 * 		        E |- case sumExpression
	 *                      inl (T1+T2) inlVar => inlExpression
	 *                      inr (T1+T2) inrVariable => inrExpression : T
	 *
	 */
	public Expression sumExpression;

	public SumType sumType;
	public Variable inlVariable;
	public Expression inlExpression;
	public Variable inrVariable;
	public Expression inrExpression;
	@Override
	public Type getType(Environment e) {
		Environment envLeft = e.clone();
		envLeft.env.put(inlVariable.varName, sumType.left);
		Type tLeft= inlExpression.getType(envLeft);
		
		Environment envRight = e.clone();
		envRight.env.put(inrVariable.varName, sumType.right);
		Type tRight= inrExpression.getType(envRight);
		
		if(!tLeft.equals(tRight)) new RuntimeException("blabla");
		
		return tRight;
	}
}
