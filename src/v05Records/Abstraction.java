package v05Records;
import java.util.HashSet;
import java.util.Set;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

public class Abstraction extends Expression {
	public String paramName;
	public Type paramType;
	public Expression body;
	
	public Abstraction(String paramName, Type paramType, Expression body) {
		super();
		this.paramName = paramName;
		this.paramType = paramType;
		this.body = body;
	}

	@Override
	public Expression reduce() {
		return null;
	}
	
	public Expression reduceWith(Expression appliedParameter) {
		return body.substituteWith(paramName, appliedParameter);
	}	
	
	public Set FI() {
		Set ret = new HashSet();
		body.FI().remove(paramName);
		return ret;
	}

	@Override
	public Expression substituteWith(String aName, Expression exp) {
		if (!paramName.equals(aName) &&  !exp.FI().contains(paramName))
			body = body.substituteWith(aName, exp);
		return this;
	}

	@Override
	public boolean isReducible() {
		return false;
	}

	/**
	 *               e1 = (E, x:T) |-  body: tbody
	 * T-Abs ========================================
	 * 		        E |- λx:T.body : T -> tbody

	 *
	 *               body.getType(E, x:T) = tbody
	 * T-Abs ========================================
	 * 		        (λx:T.body).getType(E) = new FunctionType(T,tbody)
	 *
	 */
	@Override
	public Type getType(Environment e) {
		// 1. Erzeuge eine Kopie von e
		// Füge der Kopie die Info x:T hinzu
		// ab jetzt mach alles "unterhalb" mit Kopie
		
		Environment e1 = e.clone();
		e1.env.put(this.paramName, this.paramType);
		
		Type tbody = body.getType(e1);
		
//		System.out.println("paramType: " + this.paramType);
//		System.out.println("tBody: " + tbody);
		
		return new FunctionType(this.paramType, tbody);
	}
}
