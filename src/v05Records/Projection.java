/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package v05Records;

import java.util.Set;

/*
   Syntax: {l=1}.l 
 */
public class Projection extends Expression {

	public Expression expression;
	public String label;
	
	public Projection(Expression target, String label) {
		super();
		this.expression = target;
		this.label = label;
	}

	@Override
	public Expression reduce() {
		if (expression.isReducible()) {
			expression = expression.reduce();
			return this;
		}
		
		Record rec = (Record) expression;
		return rec.elements.get(label);

	}

	@Override
	public Expression substituteWith(String aName, Expression exp) {
		expression = expression.substituteWith(aName, exp);
		return this;
	}

	@Override
	public boolean isReducible() {
		return true;
	}

	@Override
	public Set FI() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 *     E |- t:{l1: T1.....ln:Tn}
	 *     =========================
	 * 		E |- t.li : Ti 
	 */
	@Override
	public Type getType(Environment e) {
		Type t = expression.getType(e);
		
		if (!(t instanceof RecordType))
			throw new RuntimeException("Invalid Type");
		
		RecordType recType = (RecordType) t;
		if (!recType.elements.containsKey(label))
			throw new RuntimeException("Label in Projection not contained");
		
		return recType.elements.get(label);
	}

}
