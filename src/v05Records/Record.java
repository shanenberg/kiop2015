/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package v05Records;

import java.util.HashMap;
import java.util.Set;

/*
  zB { x = 12}, {x = + 1 1, y= AND true false} 
 */
public class Record extends Expression {

	HashMap<String, Expression> elements = new HashMap<String, Expression>();

	
	public Record() {
	}

	/**
	 *                   ti -> ti'
	 *     ===================================================
	 * 		{l1=t1, ....ln=tn} -> {l1=t1, ...li=ti'.....ln=tn} 
	 */
	public Record reduce() {
		for (String label : elements.keySet()) {
			Expression ti = elements.get(label);
			if (ti.isReducible()) {
				elements.put(label, ti.reduce());
				return this;
			}
		}
		return this;
	}

	@Override
	public Expression substituteWith(String aName, Expression exp) {
		throw new RuntimeException("not yet implemented");
	}

	@Override
	public boolean isReducible() {
		for (String label : elements.keySet()) {
			Expression ti = elements.get(label);
			if (ti.isReducible()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Set FI() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 *                   E |- ti: Ti
	 *     =================================================
	 * 		E |- {l1=t1, ....ln=tn}: {l1: T1, .... ln: Tn} 
	 */
	@Override
	public RecordType getType(Environment e) {
		RecordType type = new RecordType();
		
		for (String label : elements.keySet()) {
			Expression ti = elements.get(label);
			Type t = ti.getType(e);
			type.elements.put(label, t);
		}

		return type;
	}

}
