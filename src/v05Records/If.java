/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package v05Records;

import java.util.Set;

public class If extends Expression {

	public Expression conditionExpression;
	public Expression thenExpression;
	public Expression elseExpression;
	
	public If(Expression conditionExpression, Expression thenExpression,
			Expression elseExpression) {
		super();
		this.conditionExpression = conditionExpression;
		this.thenExpression = thenExpression;
		this.elseExpression = elseExpression;
	}

	/*              t -> t'
	 * ---------------------------------------------
	 * if t then t2 else t3 -> if t' then t2 else t3  
	 */

	public Expression reduce() {
		
		if (conditionExpression.isReducible()) {
			conditionExpression = conditionExpression.reduce();
			return this;
		}
		
		// E-ifTrue
		if (((Variable) conditionExpression).varName.equals("true")) {
			return new Inl(
					new SumType(
						thenExpression.getType(Environment.createEnvironment()),
						elseExpression.getType(Environment.createEnvironment())
					), thenExpression);
		}

		if (((Variable) conditionExpression).varName.equals("false")) {
			return new Inr(
					new SumType(
						thenExpression.getType(Environment.createEnvironment()),
						elseExpression.getType(Environment.createEnvironment())
					), elseExpression);
		}
		
		throw new RuntimeException("Condition der If-Bedingung hat sich nicht richtig reduziert -- weder true noch false");
	}

	
	public Expression substituteWith(String aName, Expression exp) {
		conditionExpression = conditionExpression.substituteWith(aName, exp);
		thenExpression = thenExpression.substituteWith(aName, exp);
		elseExpression = elseExpression.substituteWith(aName, exp);
		
		return this;
	}

	@Override
	public boolean isReducible() {
		return true;
	}

	@Override
	public Set FI() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * 
	 *        E |- v:Bool   E|- t1: T1   E|-t2:T2
	 * T-If ===========================================
	 *        E |- if v then t1 t else t2: T1+T2
	 */
	
	@Override
	public Type getType(Environment e) {
		Type conditionType = conditionExpression.getType(e);
		Type thenType = thenExpression.getType(e);
		Type elseType = elseExpression.getType(e);
		
		if (!new Boolean().equals(conditionType)) throw new RuntimeException("Geh wech");
		
		return new SumType(thenType, elseType);
	}

}
